import { Component } from '@angular/core';
import { ComicService } from './comic.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {NgbdModalComponent} from './modal/modal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title:string = '';
  public imagen:string = '';

  constructor( private comicService:ComicService, private modalService: NgbModal)
  {
    let comic = Math.floor((Math.random() * (619-610)) + 610);
    this.comicService.getJson(comic+'/info.0.json').subscribe((resp: any) =>{
      console.log(resp) 
      this.imagen = resp.img
      this.title = resp.title
    })

  }

  open() {
    const modalRef = this.modalService.open(NgbdModalComponent);
    modalRef.componentInstance.title = this.title;
  }


}
