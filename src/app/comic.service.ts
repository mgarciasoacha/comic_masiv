import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ComicService {


  
  constructor(private http: HttpClient) {
    console.log('Comic');
  }

  getJson(url: string) {
    return this.http.get(url);
  }
}
